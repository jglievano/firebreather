//
//  StringConstants.h
//  Chili
//
//  Auto-generated file using generate-strings.py
//
//  Copyright (c) 2013 Eosch. All rights reserved.
//

static NSString * const kStrFirebreather = @"firebreather";
static NSString * const kStrDojo = @"dojo";
static NSString * const kStrCourtyard = @"courtyard";
static NSString * const kStrImperialGardens = @"imperial_gardens";
static NSString * const kStrAteOneEgg = @"ate_one_egg";
static NSString * const kStrAteOneHundredEggs = @"ate_one_hundred_eggs";
