//
//  Constants.h
//  firebreather
//
//  Created by Gabriel Lievano on 6/18/12.
//  Copyright (c) 2012 Eosch. All rights reserved.
//

static NSString * const kStrDragonGrow = @"com.eosch.firebreather.grow";
// Legacy: We have to keep "*topscore" because there's where we started recording the time scores.
static NSString * const kStrTopTime = @"com.eosch.firebreather.%@.topscore";
static NSString * const kStrTopEggs = @"com.eosch.firebreather.%@.topeggs";
static NSString * const kStrUnlockLevel = @"com.eosch.firebreather.unlock.%@";
static NSString * const kStrTotalEggs = @"com.eosch.firebreather.%@.total_eggs";
static NSString * const kStrTutorialStep = @"com.eosch.firebreather.tutorial.step.%d";

#define kCrashZValue 1001
#define kDragonHeadZValue 1000
#define kDragonTailZValue 999
#define kEggShellZValue 100
#define kFriedEggZValue 50
#define kFireballZValue 101
#define kDragonHeadTag 10
#define kDragonTailTag 11
#define kEggShellTag 200
#define kFriedEggTag 201
#define kFireballTag 1000
#define kGameLayerTag 9999
#define kHUDLayerTag 9998
#define kPauseLayerTag 9997
#define kGameOverLayerTag 9996

#define kMainMenuTagValue 1000
#define kSceneMenuTagValue 2000

typedef enum {
  kObjectStateSpawn,
  kObjectStateExit,
  kObjectStateIdle,
  kObjectStateDead,
  kObjectStateMoving,
  kObjectStateBreaking,
  kObjectStateFried,
} ObjectState;

typedef enum {
  kCollisionTypeNone = 0,
  kCollisionTypeCircle = 1,
  kCollisionTypeSquare = 2,
  kCollisionTypeOuterCircle = 3,
  kCollisionTypeOuterSquare = 4,
} CollisionType;

typedef enum {
  kNoSceneUnininitialized = 0,
  kMainMenuScene = 1,
  kOptionsScene = 2,
  kCreditsScene = 3,
  kIntroScene = 4,
  
  kGameLevelDojo = 101,
  kGameLevelImperialGardens = 102,
  kGameLevelCourtyard = 103,
} SceneType;

typedef enum {
  kObjectTypeNone,
  kObjectTypeCrash,
  kObjectTypeDragonHead,
  kObjectTypeDragonTail,
  kObjectTypeDragonFireball,
  kObjectTypeEggShell,
  kObjectTypeFriedEgg,
  kObjectTypeGround
} GameObjectType;

@protocol GameLayerDelegate

- (void)createObjectOfType:(GameObjectType)objectType
                atLocation:(CGPoint)spawnLocation
              withVelocity:(CGPoint)initialVelocity
                withZValue:(int)zValue;

@end
