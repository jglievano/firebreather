//
//  NSString+ChiliAdditions.m
//  Chili
//
//  Created by Gabriel Liévano on 8/3/13.
//  Copyright (c) 2013 Eosch. All rights reserved.
//

#import "NSString+ChiliAdditions.h"

#import "StringConstants.h"

@implementation NSString (ChiliAdditions)

+ (NSString *)stringWithId:(NSString *)strId {
  return NSLocalizedString(strId, @"String with Id");
}

@end
