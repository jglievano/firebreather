//
//  NSString+ChiliAdditions.h
//  Chili
//
//  Created by Gabriel Liévano on 8/3/13.
//  Copyright (c) 2013 Eosch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ChiliAdditions)

+ (NSString *)stringWithId:(NSString *)strId;

@end
