//
//  GameLayer.m
//  firebreather
//
//  Created by Gabriel Lievano on 6/16/12.
//  Copyright (c) 2012 Eosch. All rights reserved.
//

#import "GameLayer.h"

#import "Crash.h"
#import "DragonHead.h"
#import "DragonTail.h"
#import "EggShell.h"
#import "Fireball.h"
#import "FriedEgg.h"
#import "GameCenterManager.h"
#import "GameManager.h"
#import "GameOverLayer.h"
#import "HUDLayer.h"
#import "PauseLayer.h"
#import "StringConstants.h"

#import "LocalyticsSession.h"
#import "SimpleAudioEngine.h"

@interface GameLayer ()

@property (nonatomic, retain) HUDLayer *hud;
@property (nonatomic, retain) PauseLayer *pauseLayer;
@property (nonatomic, retain) GameOverLayer *gameOverLayer;
@property (nonatomic, retain) NSString *level;
@property (nonatomic, retain) NSMutableArray *tails;

@end

@implementation GameLayer {
  // Layer specifics.
  CGSize _screenSize;
  ccTime _timer;
  CCSpriteBatchNode *_gameSpriteBatchNode;
  CCSpriteBatchNode *_backgroundBatchNode;
  
  // Limits.
  CGRect _limitRect;
  LimitType _limitType;
  NSMutableArray *_limitItems;
  
  // Game objects.
  DragonHead *_dragonHead;
  NSMutableArray *_eggShells;
  NSMutableArray *_fireballs;
  NSMutableArray *_friedEggs;
  int _totalEggsEatenInLevel;
  Crash *_crash;
  
  // Interaction.
  GameMode _gameMode;
  BOOL _isPaused;
  BOOL _isTouching;
  CGPoint _touchLocation;
}

static NSString * const kStrLGameOver = @"Game Over";
static NSString * const kStrLTime = @"Time";
static NSString * const kStrLEggs = @"Eggs";
static NSString * const kStrLLevel = @"LevelName";
static NSString * const kStrLReason = @"Reason";
static NSString * const kStrLBefore1Min = @"< 1 min";
static NSString * const kStrL1To2 = @"1 - 2 min";
static NSString * const kStrL2To3 = @"2 - 3 min";
static NSString * const kStrLAfter3Min = @"> 3 min";

static NSString * const kStrReasonOffLimits = @"Off Limits";
static NSString * const kStrReasonHitObject = @"Hit Object";
static NSString * const kStrReasonHitTail = @"Hit Tail";
static NSString * const kStrReasonStopped = @"Slowed Down";

static NSString * const kStrTextureCount = @"textureCount";
static NSString * const kStrKeyRed = @"red";
static NSString * const kStrKeyGreen = @"green";
static NSString * const kStrKeyBlue = @"blue";
static NSString * const kStrKeyType = @"type";
static NSString * const kStrKeyMode = @"mode";
static NSString * const kStrKeyUseCorners = @"useCorner";
static NSString * const kStrKeyItems = @"items";
static NSString * const kStrKeyItemName = @"name";
static NSString * const kStrKeyItemPosX = @"x";
static NSString * const kStrKeyItemPosY = @"y";

static float kSpeedIncrease = 2.0;

+ (CCScene *)sceneWithLevel:(NSString *)levelName {
	CCScene *scene = [CCScene node];
	GameLayer *layer = [GameLayer nodeWithLevel:levelName];
	[scene addChild:layer z:0 tag:kGameLayerTag];
  layer.hud = [HUDLayer node];
  [layer.hud proceedTutorial];
  [scene addChild:layer.hud z:1 tag:kHUDLayerTag];
  layer.pauseLayer = [PauseLayer node];
  layer.gameOverLayer = [GameOverLayer node];
  
	return scene;
}

+ (id)nodeWithLevel:(NSString *)levelName {
	return [[[self alloc] initWithLevel:levelName] autorelease];
}

- (id)initWithLevel:(NSString *)levelName {
  self = [super init];
  if (self) {
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeUnusedSpriteFrames];
    _level = [levelName copy];
    _tails = [[NSMutableArray alloc] init];
    _eggShells = [[NSMutableArray alloc] init];
    _friedEggs = [[NSMutableArray alloc] init];
    
    srandom(time(NULL));
    self.isTouchEnabled = YES;
    [[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self
                                                              priority:0
                                                       swallowsTouches:NO];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
      // TODO: Create sheet specifically for iPad
      [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"game_sheet.plist"];
      _gameSpriteBatchNode = [CCSpriteBatchNode batchNodeWithFile:@"game_sheet.png" capacity:100];
    } else {
      [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"game_sheet.plist"];
      _gameSpriteBatchNode = [CCSpriteBatchNode batchNodeWithFile:@"game_sheet.png" capacity:100];
    }
    [self addChild:_gameSpriteBatchNode z:1];

    NSString *levelDataPath = [[NSBundle mainBundle] pathForResource:_level ofType:@"plist"];
    NSDictionary *levelData = [[NSDictionary alloc] initWithContentsOfFile:levelDataPath];

    // Load level sprite sheets.
    [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA4444];
    int texCount = [[levelData objectForKey:kStrTextureCount] intValue];
    for (int i = 0; i < texCount; i++) {
      NSString *plistName = [NSString stringWithFormat:@"%@_%d.plist", _level, i];
      NSString *texFilename = [NSString stringWithFormat:@"%@_%d.pvr.ccz", _level, i];
      [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:plistName
                                                           textureFilename:texFilename];
    }
    _backgroundBatchNode = [CCNode node];

    [self addChild:_backgroundBatchNode z:0];
    [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];

    CCSprite *bgMain =
        [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@_main.png", _level]];
    CCSprite *bgUpper =
        [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@_upper.png", _level]];
    CCSprite *bgBottom =
        [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@_bottom.png", _level]];
    CCSprite *bgLeft =
        [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@_left.png", _level]];
    CCSprite *bgRight =
        [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@_right.png", _level]];

    [bgUpper setPosition:ccp(0, bgMain.contentSize.height / 2 + bgUpper.contentSize.height / 2)];
    [bgBottom setPosition:ccp(0, -bgMain.contentSize.height / 2 - bgBottom.contentSize.height / 2)];
    [bgLeft setPosition:ccp(-bgMain.contentSize.width / 2 - bgLeft.contentSize.width / 2 + 1, 0)];
    [bgRight setPosition:ccp(bgMain.contentSize.width / 2 + bgRight.contentSize.width / 2 - 1, 0)];
    
    [_backgroundBatchNode addChild:bgMain];
    [_backgroundBatchNode addChild:bgUpper];
    [_backgroundBatchNode addChild:bgBottom];
    [_backgroundBatchNode addChild:bgLeft];
    [_backgroundBatchNode addChild:bgRight];
    
    BOOL useCorners = [[levelData objectForKey:kStrKeyUseCorners] boolValue];
    if (useCorners) {
        CCSprite *bottomLeft = [CCSprite spriteWithSpriteFrameName:
            [NSString stringWithFormat:@"%@_bottomleft.png", _level]];
        CCSprite *bottomRight = [CCSprite spriteWithSpriteFrameName:
            [NSString stringWithFormat:@"%@_bottomright.png", _level]];
        CCSprite *upperLeft = [CCSprite spriteWithSpriteFrameName:
            [NSString stringWithFormat:@"%@_upperleft.png", _level]];
        CCSprite *upperRight = [CCSprite spriteWithSpriteFrameName:
            [NSString stringWithFormat:@"%@_upperright.png", _level]];
        [bottomLeft setPosition:
            ccp(-bgMain.contentSize.width / 2 - bottomLeft.contentSize.width / 2,
                -bgMain.contentSize.height / 2 - bottomLeft.contentSize.height / 2)];
        [bottomRight setPosition:
            ccp(bgMain.contentSize.width / 2 + bottomRight.contentSize.width / 2,
                -bgMain.contentSize.height / 2 - bottomRight.contentSize.height / 2)];
        [upperLeft setPosition:
            ccp(-bgMain.contentSize.width / 2 - upperLeft.contentSize.width / 2,
                bgMain.contentSize.height / 2 + upperLeft.contentSize.height / 2)];
        [upperRight setPosition:
            ccp(bgMain.contentSize.width / 2 + upperRight.contentSize.width / 2,
                bgMain.contentSize.height / 2 + upperRight.contentSize.height / 2)];
        [_backgroundBatchNode addChild:bottomLeft];
        [_backgroundBatchNode addChild:bottomRight];
        [_backgroundBatchNode addChild:upperLeft];
        [_backgroundBatchNode addChild:upperRight];
    }

    NSArray *items = [levelData objectForKey:kStrKeyItems];
    _limitItems = [[NSMutableArray alloc] init];
    for (NSDictionary *item in items) {
      CCSprite *itemSprite = [CCSprite spriteWithSpriteFrameName:
          [NSString stringWithFormat:@"%@.png", [item objectForKey:kStrKeyItemName]]];
      float positionX = [[item objectForKey:kStrKeyItemPosX] floatValue];
      float positionY = [[item objectForKey:kStrKeyItemPosY] floatValue];
      itemSprite.position = ccp(positionX, positionY);
      [_backgroundBatchNode addChild:itemSprite];
      [_limitItems addObject:itemSprite];
    }
    
    NSString *levelType = [levelData objectForKey:kStrKeyType];
    _limitRect = CGRectMake(-bgMain.contentSize.width / 2.0,
                           -bgMain.contentSize.height / 2.0,
                           bgMain.contentSize.width,
                           bgMain.contentSize.height);
    if ([levelType isEqualToString:@"LEVELTYPE_CIRCLE"]) {
      _limitType = kLimitTypeRound;
    } else if ([levelType isEqualToString:@"LEVELTYPE_RECT"]) {
      _limitType = kLimitTypeRectangle;
    } else {
      CCLOG(@"Level type %@ is not known.", levelType);
    }

    NSString *levelMode = [levelData objectForKey:kStrKeyMode];
    if ([levelMode isEqualToString:@"LEVELMODE_NORMAL"]) {
      _gameMode = kGameModeNormal;
    } else if ([levelMode isEqualToString:@"LEVELMODE_SLOW"]) {
      _gameMode = kGameModeSlow;
    } else if ([levelMode isEqualToString:@"LEVELMODE_FAST"]) {
      _gameMode = kGameModeFast;
    } else {
      CCLOG(@"Level mode %@ is not known.", levelMode);
      [NSException raise:@"ChiliException" format:@"game mode not specified."];
    }

    _screenSize = [CCDirector sharedDirector].winSize;
    [self createObjectOfType:kObjectTypeDragonHead
                  atLocation:CGPointMake(CGRectGetMidX(bgMain.boundingBox),
                                         CGRectGetMidY(bgMain.boundingBox))
                withVelocity:CGPointMake(100, 100)
                  withZValue:kDragonHeadZValue];
    for (unsigned int i = 0; i < 3; i++) {
        [self createObjectOfType:kObjectTypeDragonTail
                      atLocation:CGPointMake(CGRectGetMidX(bgMain.boundingBox),
                                             CGRectGetMidY(bgMain.boundingBox))
                    withVelocity:CGPointMake(100, 100)
                      withZValue:kDragonTailZValue];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(growDragon)
                                                 name:kStrDragonGrow
                                               object:nil];

    NSString *totalEggsKey =
        [NSString stringWithFormat:kStrTotalEggs, [GameManager sharedManager].currentLevel];
    _totalEggsEatenInLevel =
        [[[NSUserDefaults standardUserDefaults] objectForKey:totalEggsKey] intValue];

    [self scheduleUpdate];
  }
  return self;
}

- (void)cleanup {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [_dragonHead release];
  [_level release];
  [_tails release];
  [_eggShells release];
  [_friedEggs release];
  [_limitItems release];
  [_gameSpriteBatchNode release];
}

- (void)pause {
  if (!_isPaused) {
    [self pauseSchedulerAndActions];
    [self.parent addChild:self.pauseLayer z:2 tag:kPauseLayerTag];
    _isPaused = YES;
  }
}

- (void)resume {
  if (_isPaused) {
    [self resumeSchedulerAndActions];
    [self.parent removeChildByTag:kPauseLayerTag cleanup:NO];
    _isPaused = NO;
  }
}

- (void)endGame:(NSString *)reason {
  if (!_isPaused) {
    NSString *timeSpecifier;
    int minutes = _timer / 60;
    if (minutes < 1) {
      timeSpecifier = kStrLBefore1Min;
    } else if (minutes < 2) {
      timeSpecifier = kStrL1To2;
    } else if (minutes < 3) {
      timeSpecifier = kStrL2To3;
    } else {
      timeSpecifier = kStrLAfter3Min;
    }

    NSDictionary *attributes = @{
      kStrLTime : timeSpecifier,
      kStrLEggs : [NSString stringWithFormat:@"%d", _dragonHead.fedFood],
      kStrLReason : reason,
      kStrLLevel : _level
    };
    [[LocalyticsSession shared] tagEvent:kStrLGameOver
                              attributes:attributes];
    _isPaused = YES;
    [self pauseSchedulerAndActions];
    self.gameOverLayer.scoreType = kScoreTypeEggs;
    self.gameOverLayer.score = (unsigned int)_timer;
    self.gameOverLayer.eggsEaten = _dragonHead.fedFood;
    [self addEggs:_dragonHead.fedFood];
    [self.parent addChild:self.gameOverLayer z:2 tag:kGameOverLayerTag];
  }
}

- (void)addEggs:(int)amount {
  NSString *totalEggsKey =
      [NSString stringWithFormat:kStrTotalEggs, [GameManager sharedManager].currentLevel];
  int totalEggs =
      [[[NSUserDefaults standardUserDefaults] objectForKey:totalEggsKey] intValue];
  [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:(totalEggs + amount)]
                                            forKey:totalEggsKey];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -
#pragma mark GameLayerDelegate

- (void)createObjectOfType:(GameObjectType)objectType
                atLocation:(CGPoint)spawnLocation
              withVelocity:(CGPoint)initialVelocity
                withZValue:(int)zValue {
  if (objectType == kObjectTypeDragonHead) {
    CCLOG(@"Creating a Dragon Head.");
    _dragonHead = [[DragonHead alloc] init];
    _dragonHead.position = spawnLocation;
    _dragonHead.velocity = initialVelocity;
    [_gameSpriteBatchNode addChild:_dragonHead
                                z:zValue 
                              tag:kDragonHeadTag];
  } else if (objectType == kObjectTypeDragonTail) {
    CCLOG(@"Creating a Dragon Tail.");
    DragonTail *dragontail = [[DragonTail alloc] init];
    dragontail.position = spawnLocation;
    dragontail.velocity = initialVelocity;
    if ([_tails count] == 0) {
      DragonHead *dragonhead = (DragonHead *)[_gameSpriteBatchNode getChildByTag:kDragonHeadTag];
      dragontail.target = dragonhead;
    } else {
      DragonTail *prevTail = [_tails lastObject];
      dragontail.target = prevTail;
    }
    // Indicate that this tail piece was grown and will be collidable.
    if (_tails.count > 2) {
      dragontail.isGrown = YES;
    }
    [_gameSpriteBatchNode addChild:dragontail
                                 z:(zValue - [_tails count])
                               tag:kDragonTailTag];
    [_tails addObject:dragontail];
    [dragontail release];
  } else if (objectType == kObjectTypeEggShell) {
    CCLOG(@"Creating an Egg Shell at %@.", NSStringFromCGPoint(spawnLocation));
    EggShell *eggShell = [[EggShell alloc] init];
    eggShell.position = spawnLocation;
    eggShell.velocity = initialVelocity;
    if (_eggShells.count < 13) {
      eggShell.eggType = kEggTypeRock;
    } else if (arc4random() % 100 < 80) {
      eggShell.eggType = kEggTypeGood;
    } else {
      eggShell.eggType = kEggTypeRotten;
    }
    [_gameSpriteBatchNode addChild:eggShell
                                 z:zValue
                               tag:kEggShellTag];
    [_eggShells addObject:eggShell];
    [eggShell release];
  } else if (objectType == kObjectTypeDragonFireball) {
    CCLOG(@"Creating a Fireball.");
    Fireball *fireball = [[Fireball alloc] init];
    fireball.position = spawnLocation;
    fireball.velocity = initialVelocity;
    fireball.speed = _dragonHead.speed + 40;
    [_gameSpriteBatchNode addChild:fireball
                                 z:zValue
                               tag:kFireballTag];
    [_fireballs addObject:fireball];
    [fireball release];
  } else if (objectType == kObjectTypeFriedEgg) {
    CCLOG(@"Creating a Fried Egg.");
    [[SimpleAudioEngine sharedEngine] playEffect:@"eggbreak.mp3"];
    FriedEgg *friedEgg = [[FriedEgg alloc] init];
    friedEgg.position = spawnLocation;
    friedEgg.velocity = initialVelocity;
    [_gameSpriteBatchNode addChild:friedEgg
                                 z:zValue
                               tag:kFriedEggTag];
  } else if (objectType == kObjectTypeCrash) {
    CCLOG(@"Creating a Crash.");
    if (_crash) {
      CCLOG(@"Crash already exits");
      [NSException raise:@"com.eosch.firebreather.duplicatecrash" format:@"Crash created twice."];
    }
    spawnLocation.x += _dragonHead.velocity.x * 10;
    spawnLocation.y += _dragonHead.velocity.y * 10;
    _crash = [[Crash alloc] init];
    _crash.position = spawnLocation;
    _crash.velocity = initialVelocity;
    [_gameSpriteBatchNode addChild:_crash z:zValue];
  }
}

#pragma mark -
#pragma mark Update

- (void)growDragon {
  _totalEggsEatenInLevel++;
  if (_totalEggsEatenInLevel <= 100) {
    if ([_level isEqualToString:kStrDojo]) {
      NSString *achievementKey = [NSString stringWithFormat:kStrUnlockLevel, @"imperialgardens"];
      [[GameCenterManager sharedManager] saveAndReportAchievement:achievementKey
                                                  percentComplete:_totalEggsEatenInLevel
                                        shouldDisplayNotification:YES];
    } else if ([_level isEqualToString:kStrImperialGardens]) {
      NSString *achievementKey = [NSString stringWithFormat:kStrUnlockLevel, @"courtyard"];
      [[GameCenterManager sharedManager] saveAndReportAchievement:achievementKey
                                                  percentComplete:_totalEggsEatenInLevel
                                        shouldDisplayNotification:YES];
    }
    if (_totalEggsEatenInLevel == 100) {
      [self addEggs:_dragonHead.fedFood];
    }
  }
  _hud.score++;
  if (_gameMode == kGameModeNormal) {
    _dragonHead.speed += 5;
    if (_dragonHead.speed > 600) {
      _dragonHead.speed = 600;
    }
  } else if (_gameMode == kGameModeSlow) {
    _dragonHead.speed += 20.0 + (10.0 * (50 / (_tails.count - 2)));
    if (_dragonHead.speed > 160) {
      _dragonHead.speed = 160;
    }
  } else if (_gameMode == kGameModeFast) {
    _dragonHead.speed -= 25.0;
    if (_dragonHead.speed > 600) {
      _dragonHead.speed = 500;
    } else if (_dragonHead.speed < 100) {
      _dragonHead.speed = 100;
    }
  }

  [[SimpleAudioEngine sharedEngine] playEffect:@"yum.mp3"];
  [self createObjectOfType:kObjectTypeDragonTail
                atLocation:((DragonTail *)[_tails lastObject]).position
              withVelocity:ccp(100, 100)
                withZValue:kDragonTailZValue];
}

- (void)killObject:(GameObject *)gameObject {
  switch (gameObject.tag) {
    case kFireballTag:
      [_fireballs removeObject:gameObject];
      break;
    case kEggShellTag:
      [_eggShells removeObject:gameObject];
      break;
    case kFriedEggTag:
      [_friedEggs removeObject:gameObject];
      break;
    default:
      break;
  }
  [_gameSpriteBatchNode removeChild:gameObject cleanup:YES];
}

- (BOOL)checkAllCollisionsForPosition:(CGPoint)position withRadius:(float)radius {
  if ([self checkCollisionPosition:position
                            radius:radius
                      withPosition:_dragonHead.position
                            radius:_dragonHead.collisionSize]) {
    return YES;
  }

  for (CCSprite *item in _limitItems) {
    if ([self checkCollisionPosition:position
                              radius:radius
                        withPosition:item.position
                              radius:item.boundingBox.size.width]) {
      return YES;
    }
  }
  for (EggShell *shell in _eggShells) {
    if ([self checkCollisionPosition:position
                              radius:radius
                        withPosition:shell.position
                              radius:shell.collisionSize]) {
      return YES;
    }
  }
  return NO;
}

- (BOOL)checkCollisionPosition:(CGPoint)position1
                        radius:(float)radius1
                  withPosition:(CGPoint)position2
                        radius:(float)radius2 {
  float dist = sqrt(pow(position1.x - position2.x, 2) +
                    pow(position1.y - position2.y, 2));
  if (dist <= radius1 / 2 + radius2 / 2) {
    return YES;
  }
  
  return NO;
}

- (void)update:(ccTime)deltaTime {
  if (_isPaused) {
    return;
  }

  if (_crash) {
    [_crash updateStateWithDeltaTime:deltaTime andListOfGameObjects:nil];
    if (_crash.state == kObjectStateExit) {
      [self endGame:_crash.reason];
    }
    return;
  }

  _timer += deltaTime;
  self.hud.timer = _timer;

  // Update dragon.
  if (_dragonHead) {
    if (_gameMode == kGameModeSlow) {
      float deltaMultiplier = 1.0;
      if (_dragonHead.speed < 100) {
        deltaMultiplier += 20 * (1 - (_dragonHead.speed / 120));
      }
      _dragonHead.speed -= (kSpeedIncrease * deltaTime * deltaMultiplier);

      if (_dragonHead.speed <= 0) {
        [self endGame:kStrReasonStopped];
        return;
      }
    } else if (_gameMode == kGameModeFast) {
      _dragonHead.speed += kSpeedIncrease * deltaTime * 3.5;
    }

    // Update direction.
    _dragonHead.velocity = CGPointMake(_touchLocation.x - _screenSize.width / 2,
                                      _screenSize.height / 2 - _touchLocation.y);
    float m = sqrtf(powf(_dragonHead.velocity.x * _dragonHead.velocity.x, 2) +
                      powf(_dragonHead.velocity.y * _dragonHead.velocity.y, 2));
    _dragonHead.velocity = ccp(_dragonHead.velocity.x / m * 100,
                               _dragonHead.velocity.y / m * 100);
    // Check if off-limits.
    switch (_limitType) {
      case kLimitTypeRectangle: {
        CGRect offLimitsRect = CGRectInset(_limitRect, 40, 40);
        if (!CGRectIntersectsRect(_dragonHead.boundingBox, offLimitsRect)) {
          [self createObjectOfType:kObjectTypeCrash
                        atLocation:_dragonHead.position
                      withVelocity:CGPointZero
                        withZValue:kCrashZValue];
          _crash.reason = kStrReasonOffLimits;
          return;
        }
      } break;
      case kLimitTypeRound: {
        CGPoint levelCenter = CGPointMake(CGRectGetMidX(_limitRect), CGRectGetMidY(_limitRect));
        if (sqrtf(powf(_dragonHead.position.x - levelCenter.x, 2) +
                  powf(_dragonHead.position.y - levelCenter.y, 2)) >
             _limitRect.size.width / 2 - 35) {
          [self createObjectOfType:kObjectTypeCrash
                        atLocation:_dragonHead.position
                      withVelocity:CGPointZero
                        withZValue:kCrashZValue];
          _crash.reason = kStrReasonOffLimits;
          return;
        }
      } break;
      default:
        break;
    }

    // Check if collides with world objects.
    for (CCSprite *item in _limitItems) {
      if (sqrtf(powf(_dragonHead.position.x - item.position.x, 2) +
                powf(_dragonHead.position.y - item.position.y, 2)) <
          _dragonHead.collisionSize / 2 + item.boundingBox.size.width / 2) {
        [self createObjectOfType:kObjectTypeCrash
                      atLocation:_dragonHead.position
                    withVelocity:CGPointZero
                      withZValue:kCrashZValue];
        _crash.reason = kStrReasonHitObject;
        return;
      }
    }
  }

  // Real time processing
  CCArray *listOfGameObjects = [_gameSpriteBatchNode children];
  for (GameObject *tmpObject in listOfGameObjects) {
    [tmpObject updateStateWithDeltaTime:deltaTime andListOfGameObjects:listOfGameObjects];
  }

  // Check if dragon died.
  if (_dragonHead.state == kObjectStateDead) {
    [self createObjectOfType:kObjectTypeCrash
                  atLocation:_dragonHead.position
                withVelocity:CGPointZero
                  withZValue:kCrashZValue];
    _crash.reason = kStrReasonHitTail;
    return;
  }

  // Clean objects in exit state.
  for (GameObject *tmpObject in listOfGameObjects) {
    if (tmpObject.state == kObjectStateExit) {
      [self killObject:tmpObject];
    }
  }
  
  // Update camera
  if (_dragonHead) {
    CGPoint center = [_dragonHead position];
    float newX = -(_screenSize.width / 2.0) + center.x;
    float newY = -(_screenSize.height / 2.0) + center.y;
    
    [self.camera setCenterX:newX centerY:newY centerZ:0];
    [self.camera setEyeX:newX eyeY:newY eyeZ:10];
  }
  
  while ([_eggShells count] < 15) {
    CGPoint location = CGPointZero;
    do {
      if (_limitType == kLimitTypeRectangle) {
        location = CGPointMake(_limitRect.origin.x + 40, _limitRect.origin.y + 40);
        location.x += arc4random() % ((int)_limitRect.size.width - 40);
        location.y += arc4random() % ((int)_limitRect.size.height - 40);
      } else {
        location = CGPointMake(
            -_limitRect.size.width / 2.0 + (arc4random() % (int)_limitRect.size.width),
            -_limitRect.size.height / 2.0 + (arc4random() % (int)_limitRect.size.height));
        float magnitude = sqrtf((location.x * location.x) + (location.y * location.y));
        location = CGPointMake(location.x / magnitude, location.y / magnitude);
        float randomizerX = (float)((int)arc4random() % 100) / 100.0f;
        float randomizerY = (float)((int)arc4random() % 100) / 100.0f;
        location = CGPointMake(
            location.x * ((_limitRect.size.width / 2.0) - 40) * randomizerX,
            location.y * ((_limitRect.size.height / 2.0) - 40) * randomizerY);
      }
    } while ([self checkAllCollisionsForPosition:location withRadius:80]);
    
    [self createObjectOfType:kObjectTypeEggShell
                  atLocation:location
                withVelocity:CGPointMake(0, 0)
                  withZValue:kEggShellZValue];
  }
}

# pragma mark -
# pragma mark Interactivity

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
  if (_isPaused) {
    return NO;
  }
  [_hud proceedTutorial];
  _touchLocation = [touch locationInView:[touch view]];
  _isTouching = YES;
  return YES;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
  if (_isPaused) {
    return;
  }
  _touchLocation = [touch locationInView:[touch view]];
  _isTouching = YES;
}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
  if (_isPaused) {
    return;
  }
  _isTouching = NO;
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
  if (_isPaused) {
    return;
  }
  [_hud proceedTutorial];
  DragonHead *dragonhead = (DragonHead *)[_gameSpriteBatchNode getChildByTag:kDragonHeadTag];
  [self createObjectOfType:kObjectTypeDragonFireball
                atLocation:dragonhead.position
              withVelocity:dragonhead.velocity
                withZValue:kFireballZValue];
  [[SimpleAudioEngine sharedEngine] playEffect:@"flame.wav"];
  _isTouching = NO;
}

@end
