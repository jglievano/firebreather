//
//  IntroLayer.h
//  Chili
//
//  Created by Gabriel Liévano on 4/21/13.
//  Copyright Eosch 2013. All rights reserved.
//

#import "cocos2d.h"

// Layer that displays some intro to the game, previous to the main menu.
@interface IntroLayer : CCLayer

+ (CCScene *)scene;

@end
