//
//  PauseLayer.m
//  Chili
//
//  Created by Gabriel Liévano on 2/19/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "PauseLayer.h"

#import "GameManager.h"

#import "cocos2d.h"

@implementation PauseLayer

static NSString * const kStrPaused = @"PAUSED";
static NSString * const kStrFont = @"Korner_50.fnt";

- (id)init {
  self = [super initWithColor:ccc4(0, 0, 0, 180)];
  if (self) {
    self.contentSize = [CCDirector sharedDirector].winSize;

    CCSprite *bg = [CCSprite spriteWithFile:@"popup.png"];
    bg.position = ccp([CCDirector sharedDirector].winSize.width / 2,
                      [CCDirector sharedDirector].winSize.height / 2);
    [self addChild:bg];

    CCLabelBMFont *pausedLabel = [CCLabelBMFont labelWithString:kStrPaused fntFile:kStrFont];
    pausedLabel.position = ccp([CCDirector sharedDirector].winSize.width / 2,
                               [CCDirector sharedDirector].winSize.height / 2 + 10);
    [self addChild:pausedLabel];

    CCMenuItemImage *resumeButton = [CCMenuItemImage itemWithNormalImage:@"replay_button.png"
                                                           selectedImage:@"replay_button.png"
                                                                  target:self
                                                                selector:@selector(tapResume)];
    CCMenuItemImage *mainButton = [CCMenuItemImage itemWithNormalImage:@"mainmenu_button.png"
                                                         selectedImage:@"mainmenu_button.png"
                                                                target:self
                                                              selector:@selector(tapMainMenu)];
    CCMenu *menu = [CCMenu menuWithItems:resumeButton, mainButton, nil];
    resumeButton.position = ccp(-112, -80);
    mainButton.position = ccp(112, -80);
    [self addChild:menu];
  }
  return self;
}

- (void)tapResume {
  [[GameManager sharedManager] resumeGame];
}

- (void)tapMainMenu {
  [[GameManager sharedManager] runSceneWithID:kMainMenuScene];
}

@end
