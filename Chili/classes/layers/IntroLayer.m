//
//  IntroLayer.m
//  Chili
//
//  Created by Gabriel Liévano on 4/21/13.
//  Copyright Eosch 2013. All rights reserved.
//

#import "IntroLayer.h"

#import "MainMenuLayer.h"
#import "SimpleAudioEngine.h"

#pragma mark - IntroLayer

@implementation IntroLayer

+ (CCScene *)scene {
	CCScene *scene = [CCScene node];
	IntroLayer *layer = [IntroLayer node];
	[scene addChild:layer];
	
	return scene;
}

- (void)onEnter {
	[super onEnter];

  [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"ost.mp3"];

	// Ask director for the window size.
	CGSize size = [[CCDirector sharedDirector] winSize];

	CCSprite *background = [CCSprite spriteWithFile:@"loading.png"];
	background.position = ccp(size.width / 2, size.height / 2);

	// Add the label as a child to this Layer.
	[self addChild:background];
	
	// In one second transition to the new scene.
	[self scheduleOnce:@selector(makeTransition:) delay:1];
}

- (void)makeTransition:(ccTime)dt {
	[[CCDirector sharedDirector] replaceScene:
      [CCTransitionFade transitionWithDuration:1.0 scene:[MainMenuLayer scene] withColor:ccWHITE]];
}
@end
