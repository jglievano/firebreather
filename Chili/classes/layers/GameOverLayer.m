//
//  GameOverLayer.m
//  Chili
//
//  Created by Gabriel Liévano on 2/19/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "GameOverLayer.h"

#import "GameManager.h"

#import "cocos2d.h"

@implementation GameOverLayer {
  CCLabelBMFont *_scoreTitle;
  CCLabelBMFont *_scoreLabel;
  CCLabelBMFont *_highestScoreLabel;
  CCLabelBMFont *_timeLabel;
}

static NSString * const kStrFont = @"Korner_50.fnt";
static NSString * const kStrSmallFont = @"Korner_30.fnt";

static NSString * const kStrScoreTimeTitle = @"Elapsed Time";
static NSString * const kStrScoreEggsTitle = @"Eggs Eaten";

- (id)init {
  self = [super initWithColor:ccc4(0, 0, 0, 80)];
  if (self) {
    self.contentSize = [CCDirector sharedDirector].winSize;

    CCSprite *bg = [CCSprite spriteWithFile:@"popup.png"];
    bg.position = ccp([CCDirector sharedDirector].winSize.width / 2,
                      [CCDirector sharedDirector].winSize.height / 2);
    [self addChild:bg];

    _scoreTitle = [CCLabelBMFont labelWithString:kStrScoreTimeTitle fntFile:kStrFont];
    _scoreTitle.scale = 0.95;
    _scoreTitle.position = ccp([CCDirector sharedDirector].winSize.width / 2,
                              [CCDirector sharedDirector].winSize.height / 2 + 60);
    _scoreTitle.color = ccc3(255, 186, 0);
    [self addChild:_scoreTitle];

    CCLayerColor *scoreBG = [CCLayerColor layerWithColor:ccc4(0, 0, 0, 120) width:170 height:30];
    scoreBG.position = ccp([CCDirector sharedDirector].winSize.width / 2 - 85,
                           [CCDirector sharedDirector].winSize.height / 2 + 5);
    [self addChild:scoreBG];

    _scoreLabel = [CCLabelBMFont labelWithString:@"0" fntFile:kStrFont];
    _scoreLabel.scale = 0.95;
    _scoreLabel.position = ccp([CCDirector sharedDirector].winSize.width / 2,
                               [CCDirector sharedDirector].winSize.height / 2 + 26);
    [self addChild:_scoreLabel];

    CCLabelBMFont *highestScoreLabel = [CCLabelBMFont labelWithString:@"Top Record"
                                                              fntFile:kStrFont];
    highestScoreLabel.scale = 0.95;
    highestScoreLabel.position = ccp([CCDirector sharedDirector].winSize.width / 2,
                                     [CCDirector sharedDirector].winSize.height / 2 - 10);
    highestScoreLabel.color = ccc3(255, 186, 0);
    [self addChild:highestScoreLabel];

    _highestScoreLabel = [CCLabelBMFont labelWithString:@"0" fntFile:kStrFont];
    _highestScoreLabel.scale = 0.95;
    _highestScoreLabel.position = ccp([CCDirector sharedDirector].winSize.width / 2,
                                      [CCDirector sharedDirector].winSize.height / 2 - 42);
    [self addChild:_highestScoreLabel];

    CCMenuItemImage *restartButton = [CCMenuItemImage itemWithNormalImage:@"replay_button.png"
                                                           selectedImage:@"replay_button_on.png"
                                                                  target:self
                                                                selector:@selector(tapRestart)];
    CCMenuItemImage *mainButton = [CCMenuItemImage itemWithNormalImage:@"mainmenu_button.png"
                                                         selectedImage:@"mainmenu_button_on.png"
                                                                target:self
                                                              selector:@selector(tapMainMenu)];
    restartButton.position = ccp(-112, -80);
    mainButton.position = ccp(112, -80);
    CCMenu *menu = [CCMenu menuWithItems:restartButton, mainButton, nil];
    [self addChild:menu];
  }
  return self;
}

- (void)onEnter {
  [super onEnter];
}

- (void)onExit {
  [super onExit];
}

- (void)setScoreType:(scoringType)scoreType {
  _scoreType = scoreType;
  if (_scoreType == kScoreTypeTime) {
    _scoreTitle.string = kStrScoreTimeTitle;
  } else {
    _scoreTitle.string = kStrScoreEggsTitle;
  }
}

- (void)setScore:(unsigned int)score {
  NSString *topScoreKey =
      [NSString stringWithFormat:kStrTopTime, [GameManager sharedManager].currentLevel];
  int highestScore =
      [[[NSUserDefaults standardUserDefaults] objectForKey:topScoreKey] intValue];

  if (score > highestScore) {
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:score]
                                              forKey:topScoreKey];
    highestScore = score;
    [[GameManager sharedManager] reportScore:highestScore toLeaderboard:topScoreKey];
  }

  if (_scoreType == kScoreTypeTime) {
    int seconds = (int)score % 60;
    int minutes = 0;
    if (score > 0) {
      minutes = score / 60;
    }
    _scoreLabel.string = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];

    seconds = (int)highestScore % 60;
    minutes = 0;
    if (highestScore > 0) {
      minutes = highestScore / 60;
    }
    _highestScoreLabel.string = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
  }
}

- (void)setEggsEaten:(unsigned int)eggsEaten {
  NSString *topScoreKey =
      [NSString stringWithFormat:kStrTopEggs, [GameManager sharedManager].currentLevel];
  int highestScore =
      [[[NSUserDefaults standardUserDefaults] objectForKey:topScoreKey] intValue];

  if (eggsEaten > highestScore) {
    [[NSUserDefaults standardUserDefaults] setObject:@(eggsEaten)
                                              forKey:topScoreKey];
    highestScore = eggsEaten;
    [[GameManager sharedManager] reportScore:highestScore toLeaderboard:topScoreKey];
  }

  if (_scoreType == kScoreTypeEggs) {
    _scoreLabel.string = [NSString stringWithFormat:@"%d", eggsEaten];
    _highestScoreLabel.string = [NSString stringWithFormat:@"%d", highestScore];
  }
}

- (void)tapRestart {
  [[GameManager sharedManager] restartGame];
}

- (void)tapMainMenu {
  [[GameManager sharedManager] runSceneWithID:kMainMenuScene];
}

@end
