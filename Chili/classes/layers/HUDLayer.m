//
//  HUDLayer.m
//  Chili
//
//  Created by Gabriel Liévano on 2/18/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "HUDLayer.h"

#import "GameManager.h"

#import "cocos2d.h"

@implementation HUDLayer {
  int _currentTutorialStep;
  CCLabelBMFont *_scoreLabel;
  CCLabelBMFont *_timeLabel;
  CCLabelBMFont *_tutorialLabel;
  CCMenu *_hudMenu;
}

static NSString * const kStrTutorial1 = @"touch to guide your dragon";
static NSString * const kStrTutorial2 = @"release to shoot fireballs";
static NSString * const kStrTutorial3 = @"fire at eggs to cook them!";

- (id)init {
  self = [super init];
  if (self) {
    _currentTutorialStep = 0;
    CGSize screenSize = [CCDirector sharedDirector].winSize;

    _tutorialLabel = [[CCLabelBMFont labelWithString:kStrTutorial1
                                             fntFile:@"Korner_50.fnt"] retain];
    _tutorialLabel.scale = 0.9;
    _tutorialLabel.position = ccp(screenSize.width / 2, screenSize.height / 2 + 40);

    CCSprite *egg = [CCSprite spriteWithFile:@"hud_egg.png"];
    egg.position = ccp([CCDirector sharedDirector].winSize.width / 2 - 20,
                       [CCDirector sharedDirector].winSize.height - 28);
    [self addChild:egg];

    _scoreLabel = [CCLabelBMFont labelWithString:@"0" fntFile:@"Korner_50.fnt"];
    _scoreLabel.anchorPoint = ccp(0.5, 0.5);
    _scoreLabel.position = ccp([CCDirector sharedDirector].winSize.width / 2,
                               [CCDirector sharedDirector].winSize.height - 20);
    [self addChild:_scoreLabel];

    _timeLabel = [CCLabelBMFont labelWithString:@"00:00" fntFile:@"Korner_50.fnt"];
    _timeLabel.anchorPoint = ccp(0.0, 0.5);
    _timeLabel.position = ccp(30, [CCDirector sharedDirector].winSize.height - 20);
    [self addChild:_timeLabel];

    CCMenuItemImage *pauseButton = [CCMenuItemImage itemWithNormalImage:@"pause_button.png"
                                                          selectedImage:@"pause_button_on.png"
                                                                 target:self
                                                               selector:@selector(tapPause)];
    pauseButton.position = ccp(screenSize.width / 2 - 40, 130);
    _hudMenu = [CCMenu menuWithItems:pauseButton, nil];
    [self addChild:_hudMenu];
  }
  return self;
}

- (int)tutorialStep {
  return _currentTutorialStep;
}

- (void)proceedTutorial {
  if (_currentTutorialStep > 3) {
    return;
  }
  do {
    _currentTutorialStep++;
    if (_currentTutorialStep > 3) {
      return;
    }
  } while ([[GameManager sharedManager] shouldShowTutorialStep:_currentTutorialStep]);
  [[GameManager sharedManager] registerTutorialStepHasBeenShown:_currentTutorialStep];
  if (_currentTutorialStep == 1) {
    _tutorialLabel.string = kStrTutorial1;
    _tutorialLabel.color = ccc3(10, 230, 25);
    [self addChild:_tutorialLabel z:1001];
  } else if (_currentTutorialStep == 2) {
    _tutorialLabel.string = kStrTutorial2;
    _tutorialLabel.color = ccc3(245, 235, 0);
  } else if (_currentTutorialStep == 3) {
    _tutorialLabel.string = kStrTutorial3;
    _tutorialLabel.color = ccc3(255, 200, 100);
    [self scheduleOnce:@selector(dismissTutorial) delay:5.0];
  }
}

- (void)dismissTutorial {
  [self removeChild:_tutorialLabel cleanup:NO];
}

- (void)setScore:(int)score {
  _score = score;
  _scoreLabel.scale = 2.0;
  [_scoreLabel runAction:[CCScaleTo actionWithDuration:0.5 scale:1.0]];
  [_scoreLabel setString:[NSString stringWithFormat:@"%d", _score]];
}

- (void)setTimer:(ccTime)timer {
  int seconds = (int)timer % 60;
  int minutes = 0;
  if (timer > 0) {
    minutes = timer / 60;
  }
  _timeLabel.string = [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}

- (void)tapPause {
  [[GameManager sharedManager] pauseGame];
}

@end
