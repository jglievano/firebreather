//
//  GameOverLayer.h
//  Chili
//
//  Created by Gabriel Liévano on 2/19/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "CCLayer.h"

typedef NS_ENUM(NSInteger, scoringType) {
  kScoreTypeTime,
  kScoreTypeEggs,
};

@interface GameOverLayer : CCLayerColor

@property (nonatomic, setter = setScore:) unsigned int score;
@property (nonatomic, setter = setEggsEaten:) unsigned int eggsEaten;
@property (nonatomic, setter = setScoreType:) scoringType scoreType;

@end
