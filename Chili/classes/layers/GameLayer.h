//
//  GameLayer.h
//  firebreather
//
//  Created by Gabriel Lievano on 6/16/12.
//  Copyright (c) 2012 Eosch. All rights reserved.
//

#import "cocos2d.h"
#import "Constants.h"

typedef NS_ENUM(NSInteger, LimitType) {
  kLimitTypeRound,
  kLimitTypeRectangle,
};

typedef NS_ENUM(NSInteger, GameMode) {
  kGameModeNormal,
  kGameModeSlow,
  kGameModeFast,
};

@interface GameLayer : CCLayer
<GameLayerDelegate>

+ (CCScene *)sceneWithLevel:(NSString *)levelName;
+ (id)nodeWithLevel:(NSString *)levelName;
- (id)initWithLevel:(NSString *)levelName;
- (void)pause;
- (void)resume;
- (void)endGame:(NSString *)reason;

@end
