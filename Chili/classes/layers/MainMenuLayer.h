//
//  MainMenuLayer.h
//  firebreather
//
//  Created by Gabriel Lievano on 11/3/12.
//
//

#import <Foundation/Foundation.h>

#import "cocos2d.h"
#import <GameKit/GameKit.h>

#import "Constants.h"
#import "GameManager.h"

@interface MainMenuLayer : CCLayer
<GKAchievementViewControllerDelegate, GKLeaderboardViewControllerDelegate>

+ (CCScene *)scene;

@end
