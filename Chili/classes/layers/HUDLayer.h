//
//  HUDLayer.h
//  Chili
//
//  Created by Gabriel Liévano on 2/18/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "CCLayer.h"

@interface HUDLayer : CCLayer

@property (nonatomic, assign) int score;
@property (nonatomic, setter = setTimer:) ccTime timer;
@property (nonatomic, readonly) int tutorialStep;

- (void)proceedTutorial;
- (void)dismissTutorial;

@end
