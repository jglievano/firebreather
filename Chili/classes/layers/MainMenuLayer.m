//
//  MainMenuLayer.m
//  firebreather
//
//  Created by Gabriel Lievano on 11/3/12.
//
//

#import "MainMenuLayer.h"

#import "AppDelegate.h"

#import "LocalyticsSession.h"

@interface MainMenuLayer ()

- (void)displayMainMenu;

@end

@implementation MainMenuLayer {
  CCMenu *_sceneSelectMenu;
  CCMenu *_mainMenu;
  CCMenu *_loading;
}

static NSString * const kStrLStartLevel = @"Start Level";
static NSString * const kStrLLevel = @"Level";
static NSString * const kStrLLevelDojo = @"Dojo";
static NSString * const kStrLLevelCourtyard = @"Courtyard";
static NSString * const kStrLLevelImperialGardens = @"Imperial Gardens";

+ (CCScene *)scene {
	CCScene *scene = [CCScene node];
	MainMenuLayer *layer = [MainMenuLayer node];
	[scene addChild:layer];
  
	return scene;
}

- (id)init {
  self = [super init];
  if (self) {
    CGSize screenSize = [CCDirector sharedDirector].winSize;
    
    CCSprite *background = [CCSprite spriteWithFile:@"mainMenu_bg.png"];
    background.position = ccp(screenSize.width / 2, screenSize.height / 2);
    [self addChild:background];
    
    [self displayMainMenu];
    
  }
  return self;
}

- (void)onEnter {
  [super onEnter];
}

- (void)onExit {
  [super onExit];
}

- (void)playScene:(CCMenuItemFont *)itemPassedIn {
  if ([itemPassedIn tag] == 1) {
    CCLOG(@"<MainMenuLayer> playScene:CCMenuItemFont tag 1 found, scene 1");
    [[GameManager sharedManager] runSceneWithID:kIntroScene];
  } else {
    CCLOG(@"Tag was: %d", [itemPassedIn tag]);
  }
}

- (void)displayMainMenu {
  CGSize screenSize = [CCDirector sharedDirector].winSize;

  NSString *courtyard = @"sel_courtyard.png";
  NSString *courtyard_bg = @"sel_courtyard_bg.png";
  NSString *dojo = @"sel_dojo.png";
  NSString *dojo_bg = @"sel_dojo_bg.png";
  NSString *imperialgardens = @"sel_imperial_gardens.png";
  NSString *imperialgardens_bg = @"sel_imperial_gardens_bg.png";

  CCSprite *frame = [CCSprite spriteWithFile:@"mainMenu_frame.png"];
  frame.position = ccp(screenSize.width / 2 + 120, screenSize.height / 2 - 100);
  frame.scaleX = 35;
  [self addChild:frame z:90];

  CCLabelBMFont *selectLabel = [CCLabelBMFont labelWithString:@"PICK YOUR GAME HERE"
                                                      fntFile:@"Korner_50.fnt"];
  selectLabel.scale = 0.6;
  selectLabel.color = ccc3(48, 10, 9);
  selectLabel.position = ccp(screenSize.width / 2 + 60,
                             screenSize.height / 2 - 35);
  [self addChild:selectLabel];

  CCSprite *bg1 = [CCSprite spriteWithFile:@"mainMenu_circle.png"];
  bg1.position = ccp(screenSize.width / 2 - 59, screenSize.height / 2 - 100);
  bg1.scale = 1.15;
  [self addChild:bg1 z:100];
  CCSprite *bg2 = [CCSprite spriteWithFile:@"mainMenu_circle.png"];
  bg2.position = ccp(screenSize.width / 2 + 61, screenSize.height / 2 - 100);
  bg2.scale = 1.15;
  [self addChild:bg2 z:101];
  CCSprite *bg3 = [CCSprite spriteWithFile:@"mainMenu_circle.png"];
  bg3.position = ccp(screenSize.width / 2 + 181, screenSize.height / 2 - 100);
  bg3.scale = 1.15;
  [self addChild:bg3 z:102];

  CCSprite *play1BG = [CCSprite spriteWithFile:dojo_bg];
  CCMenuItemImage *play1Button =
      [CCMenuItemImage itemWithNormalImage:dojo
                             selectedImage:dojo
                                    target:self
                                  selector:@selector(playInDojo)];
  CCSprite *play2BG = [CCSprite spriteWithFile:imperialgardens_bg];
  CCMenuItemImage *play2Button =
      [CCMenuItemImage itemWithNormalImage:imperialgardens
                             selectedImage:imperialgardens
                                    target:self
                                  selector:@selector(playInImperialGardens)];
  CCMenuItemImage *play3BG = [CCSprite spriteWithFile:courtyard_bg];
  CCMenuItemImage *play3Button =
      [CCMenuItemImage itemWithNormalImage:courtyard
                             selectedImage:courtyard
                                    target:self
                                  selector:@selector(playInCourtyard)];
  int eggsToLvl2 = [self eggsToUnlockImperialGardens];
  int eggsToLvl3 = [self eggsToUnlockCourtyard];
  CCSprite *levelLock2 = [CCSprite spriteWithFile:@"sel_locked.png"];
  CCSprite *levelLock3 = [CCSprite spriteWithFile:@"sel_locked.png"];
  CCLabelBMFont *lock2Label =
      [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"requires\n%d eggs", eggsToLvl2]
                             fntFile:@"Korner_30.fnt"];
  CCLabelBMFont *lock3Label =
      [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"requires\n%d eggs", eggsToLvl3]
                             fntFile:@"Korner_30.fnt"];
  play1BG.position = ccp(screenSize.width / 2 - 60, screenSize.height / 2 - 100);
  play1Button.position = ccp(-60, -100);
  levelLock2.position = ccp(screenSize.width / 2 + 60, screenSize.height / 2 - 100);
  lock2Label.position = ccp(screenSize.width / 2 + 60, screenSize.height / 2 - 95);
  play2BG.position = ccp(screenSize.width / 2 + 60, screenSize.height / 2 - 100);
  play2Button.position = ccp(60, -100);
  levelLock3.position = ccp(screenSize.width / 2 + 180, screenSize.height / 2 - 100);
  lock3Label.position = ccp(screenSize.width / 2 + 180, screenSize.height / 2 - 95);
  play3BG.position = ccp(screenSize.width / 2 + 180, screenSize.height / 2 - 100);
  play3Button.position = ccp(180, -100);

  CCMenuItemImage *gameCenter = [CCMenuItemImage itemWithNormalImage:@"gc.png"
                                                       selectedImage:@"gc_on.png"
                                                               block:^(id sender) {
    [[GameManager sharedManager] showLeaderboard];
  }];

  gameCenter.position = ccp(-screenSize.width / 2 + gameCenter.boundingBox.size.width / 2, -100);

  _sceneSelectMenu = [CCMenu menuWithItems:
    play1Button,
    play2Button,
    play3Button,
    gameCenter,
    nil
  ];
  [play1BG runAction:[CCRepeatForever actionWithAction:
      [CCRotateBy actionWithDuration:10.0 angle:360]]];
  [play2BG runAction:[CCRepeatForever actionWithAction:
      [CCRotateBy actionWithDuration:10.0 angle:360]]];
  [play3BG runAction:[CCRepeatForever actionWithAction:
      [CCRotateBy actionWithDuration:10.0 angle:360]]];
  [self addChild:play1BG z:103];
  [self addChild:play2BG z:104];
  [self addChild:play3BG z:105];
  if (eggsToLvl2 > 0) {
    [self addChild:levelLock2 z:110];
    [self addChild:lock2Label z:111];
  }
  if (eggsToLvl3 > 0) {
    [self addChild:levelLock3 z:110];
    [self addChild:lock3Label z:111];
  }
  [_sceneSelectMenu setPosition:ccp(screenSize.width / 2, screenSize.height / 2)];
  [self addChild:_sceneSelectMenu z:108 tag:kSceneMenuTagValue];

  _loading = [[CCSprite spriteWithFile:@"popup.png"] retain];
  _loading.position = ccp(screenSize.width / 2, screenSize.height / 2);
  _loading.opacity = 0;
  CCLabelBMFont *loadingLabel = [CCLabelBMFont labelWithString:@"LOADING"
                                                       fntFile:@"Korner_50.fnt"];
  loadingLabel.position = ccp(_loading.boundingBox.size.width / 2,
                              _loading.boundingBox.size.height / 2 + 10);
  [_loading addChild:loadingLabel];
}

- (void)playInCourtyard {
  if ([self eggsToUnlockCourtyard] > 100) {
    return;
  }
  [self addChild:_loading z:202];
  CCFadeIn *fadeIn = [CCFadeIn actionWithDuration:0.1];
  CCCallBlock *block = [CCCallBlock actionWithBlock:^{
    [[LocalyticsSession shared] tagEvent:kStrLStartLevel
                              attributes:@{kStrLLevel : kStrLLevelCourtyard}];
    [[GameManager sharedManager] runSceneWithID:kGameLevelCourtyard];
  }];
  CCSequence *seq = [CCSequence actionWithArray:@[fadeIn, block]];
  [_loading runAction:seq];
}

- (void)playInDojo {
  [self addChild:_loading z:202];
  CCFadeIn *fadeIn = [CCFadeIn actionWithDuration:0.1];
  CCCallBlock *block = [CCCallBlock actionWithBlock:^{
    [[LocalyticsSession shared] tagEvent:kStrLStartLevel
                              attributes:@{kStrLLevel : kStrLLevelDojo}];
    [[GameManager sharedManager] runSceneWithID:kGameLevelDojo];
  }];
  CCSequence *seq = [CCSequence actionWithArray:@[fadeIn, block]];
  [_loading runAction:seq];
}

- (void)playInImperialGardens {
  if ([self eggsToUnlockImperialGardens] > 0) {
    return;
  }
  [self addChild:_loading z:202];
  CCFadeIn *fadeIn = [CCFadeIn actionWithDuration:0.1];
  CCCallBlock *block = [CCCallBlock actionWithBlock:^{
    [[LocalyticsSession shared] tagEvent:kStrLStartLevel
                              attributes:@{kStrLLevel : kStrLLevelImperialGardens}];
    [[GameManager sharedManager] runSceneWithID:kGameLevelImperialGardens];
  }];
  CCSequence *seq = [CCSequence actionWithArray:@[fadeIn, block]];
  [_loading runAction:seq];
}

- (int)eggsToUnlockImperialGardens {
  // We check how many eggs has been eaten in total for the previous level.
  NSString *totalEggsKey =
      [NSString stringWithFormat:kStrTotalEggs, @"dojo"];
  int totalEggs =
      [[[NSUserDefaults standardUserDefaults] objectForKey:totalEggsKey] intValue];
  return 100 - totalEggs;
}

- (BOOL)eggsToUnlockCourtyard {
  // We check how many eggs has been eaten in total for the previous level.
  NSString *totalEggsKey =
      [NSString stringWithFormat:kStrTotalEggs, @"imperialgardens"];
  int totalEggs =
      [[[NSUserDefaults standardUserDefaults] objectForKey:totalEggsKey] intValue];
  return 100 - totalEggs;
}

#pragma mark GameKit delegate

- (void)achievementViewControllerDidFinish:(GKAchievementViewController *)viewController {
	AppController *app = (AppController *)[UIApplication sharedApplication].delegate;
	[[app navController] dismissModalViewControllerAnimated:YES];
}

- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController {
	AppController *app = (AppController *)[UIApplication sharedApplication].delegate;
	[[app navController] dismissModalViewControllerAnimated:YES];
}

@end
