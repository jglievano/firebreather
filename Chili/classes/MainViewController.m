//
//  MainViewController.m
//  Chili
//
//  Created by Gabriel Liévano on 2/22/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "MainViewController.h"

@implementation MainViewController

- (void)showLeaderboard {
  [[GameCenterManager sharedManager] presentLeaderboardsOnViewController:self];
}

- (void)gameCenterManager:(GameCenterManager *)manager
         authenticateUser:(UIViewController *)gameCenterLoginController {
  [self presentViewController:gameCenterLoginController animated:YES completion:^{
    NSLog(@"Finished Presenting Authentication Controller");
  }];
}

@end
