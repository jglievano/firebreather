//
//  GameObject.h
//  firebreather
//
//  Created by Gabriel Lievano on 6/18/12.
//  Copyright (c) 2012 Eosch. All rights reserved.
//

#import "cocos2d.h"
#import "Constants.h"

typedef void (^AnimCompletionBlock)();

@interface GameObject : CCSprite

@property (readwrite) bool isActive;
@property (readwrite) bool reactsToScreenBoundaries;
@property (readwrite) CGSize screenSize;
@property (nonatomic, assign) ObjectState state;
@property (nonatomic, assign) CGPoint velocity;
@property (nonatomic, assign) float speed;
@property (readwrite) GameObjectType gameObjectType;
@property (nonatomic, assign) CollisionType collisionType;
@property (nonatomic, assign) float collisionSize;

- (void)changeState:(ObjectState)newState;
- (void)updateStateWithDeltaTime:(ccTime)deltaTime
            andListOfGameObjects:(CCArray *)listOfGameObjects;
- (CCSequence *)sequenceWithAnimation:(CCAnimation *)anim
                   andCompletionBlock:(AnimCompletionBlock)block;
- (CGRect)adjustBoundingBox;
- (BOOL)collidesWith:(GameObject *)otherObject;
- (CCAnimation *)loadPlistForAnimationWithName:(NSString *)animationName
                                 andEntityName:(NSString *)entityName;
                                 
- (BOOL)collidesWith:(GameObject *)otherObject;
- (void)lookAt:(CGPoint)target;
- (void)velocityTowards:(CGPoint)target;
- (void)rotate:(float)r;
- (void)moveWithDelta:(ccTime)deltaTime;

@end
