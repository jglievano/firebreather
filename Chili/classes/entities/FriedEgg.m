//
//  FriedEgg.m
//  Chili
//
//  Created by Gabriel Liévano on 9/29/13.
//  Copyright (c) 2013 Eosch. All rights reserved.
//

#import "FriedEgg.h"

@implementation FriedEgg {
  CCAnimation *_friedAnim;
}

- (id)init {
  self = [super initWithSpriteFrameName:@"fried_egg_1.png"];
  if (self) {
    [self startAnimations];
    self.gameObjectType = kObjectTypeFriedEgg;
    self.speed = 0.0;
    self.collisionType = kCollisionTypeCircle;
    self.collisionSize = 40;
    self.scale = 0.2;
    [self changeState:kObjectStateIdle];
  }
  return self;
}

- (void)dealloc {
  [_friedAnim release];
  [super dealloc];
}

- (void)changeState:(ObjectState)newState {
  [self stopAllActions];
  id action = nil;
  self.state = newState;
  
  if (newState == kObjectStateIdle) {
    action = [self sequenceWithAnimation:_friedAnim andCompletionBlock:^{
      [self changeState:kObjectStateFried];
    }];
    id scaleIn = [CCScaleTo actionWithDuration:0.5 scale:1.0];
    [self runAction:scaleIn];
  } else if (newState == kObjectStateFried) {
  }
}

- (void)updateStateWithDeltaTime:(ccTime)deltaTime
            andListOfGameObjects:(CCArray *)listOfGameObjects {
  if (self.state == kObjectStateDead) {
    return;
  } else if (self.state == kObjectStateIdle) {
    
  } else if (self.state == kObjectStateFried) {
  }
}

- (void)startAnimations {
  [_friedAnim autorelease];
  _friedAnim = [[self loadPlistForAnimationWithName:@"friedAnim"
                                      andEntityName:@"egg_shell"] retain];
}

@end
