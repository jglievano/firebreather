//
//  Fireball.h
//  firebreather
//
//  Created by Gabriel Lievano on 6/16/12.
//  Copyright 2012 Eosch. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GameObject.h"

@interface Fireball : GameObject

@end
