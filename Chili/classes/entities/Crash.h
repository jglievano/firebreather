//
//  Crash.h
//  Chili
//
//  Created by Gabriel Liévano on 2/22/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "GameObject.h"

@interface Crash : GameObject

@property (nonatomic, copy) NSString *reason;

@end
