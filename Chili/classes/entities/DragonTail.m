//
//  DragonTail.m
//  firebreather
//
//  Created by Gabriel Lievano on 6/18/12.
//  Copyright 2012 Eosch. All rights reserved.
//

#import "DragonTail.h"

#define kBodyRadius 18

@implementation DragonTail {
  CCAnimation *_movingAnim;
  GameObject *_target;
}

@synthesize target = _target;

- (id)init {
  self = [super initWithSpriteFrameName:@"dragonbody_1.png"];
  if (self) {
    [self startAnimations];
    self.gameObjectType = kObjectTypeDragonTail;
    self.speed = 0.0;
    self.collisionType = kCollisionTypeCircle;
    self.collisionSize = 24;
    [self changeState:kObjectStateMoving];
  }
  return self;
}

- (void)dealloc {
  [_movingAnim release];
  [super dealloc];
}

- (void)changeState:(ObjectState)newState {
  [self stopAllActions];
  id action = nil;
  self.state = newState;
  
  if (newState == kObjectStateMoving) {
    action = [CCAnimate actionWithAnimation:_movingAnim];
  } else {
    CCLOG(@"Unhandled state in DragonHead");
  }
  
  if (action) {
    [self runAction:action];
  }
}

- (void)updateStateWithDeltaTime:(ccTime)deltaTime
            andListOfGameObjects:(CCArray *)listOfGameObjects {
  if (self.state == kObjectStateDead) {
    return;
  } else if (self.state == kObjectStateMoving) {
    if (!_target)
      return;
    
    CGPoint diff = CGPointMake(_target.position.x - self.position.x,
                               _target.position.y - self.position.y);
    float dist = sqrt((diff.x * diff.x) + (diff.y * diff.y));
    
    if (dist > kBodyRadius) {
      CGPoint invDiff = CGPointMake(self.position.x - _target.position.x,
                                    self.position.y - _target.position.y);
      float invDiffM = sqrt((invDiff.x * invDiff.x) + (invDiff.y * invDiff.y));
      CGPoint invDiffN = CGPointMake(invDiff.x / invDiffM, invDiff.y / invDiffM);
      CGPoint nextPoint = CGPointMake(_target.position.x + (invDiffN.x * kBodyRadius),
                                      _target.position.y + (invDiffN.y * kBodyRadius));
      self.position = nextPoint;
    }
    if (dist != 0) {
      [self lookAt:_target.position];
    }
  }
}

- (void)startAnimations {
  [_movingAnim autorelease];
  _movingAnim = [[self loadPlistForAnimationWithName:@"movingAnim"
                                       andEntityName:@"dragon_tail"] retain];
}

@end
