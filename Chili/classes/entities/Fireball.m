//
//  Fireball.m
//  firebreather
//
//  Created by Gabriel Lievano on 6/16/12.
//  Copyright 2012 Eosch. All rights reserved.
//

#import "Fireball.h"

#import "EggShell.h"
#import "GameLayer.h"

@implementation Fireball {
  CCAnimation *_movingAnim;
}

- (id)init {
  self = [super initWithSpriteFrameName:@"fireball_1.png"];
  if (self) {
    [self startAnimations];
    self.gameObjectType = kObjectTypeDragonFireball;
    self.speed = 200.0;
    self.collisionType = kCollisionTypeCircle;
    self.collisionSize = 40;
    [self changeState:kObjectStateMoving];
  }
  return self;
}

- (void)dealloc {
  [_movingAnim release];
  [super dealloc];
}

- (void)changeState:(ObjectState)newState {
  [self stopAllActions];
  id action = nil;
  self.state = newState;
  
  if (newState == kObjectStateMoving) {
    action = [self sequenceWithAnimation:_movingAnim andCompletionBlock:^{
      [self changeState:kObjectStateMoving];
    }];
  } else if (newState == kObjectStateExit) {
    
  } else {
    CCLOG(@"Unhandled state in DragonHead");
  }
  
  if (action) {
    [self runAction:action];
  }
}

- (BOOL)collidesWith:(GameObject *)otherObject {
    // this is type circle
  if (CGRectIntersectsRect(self.boundingBox, otherObject.boundingBox)) {
    if (otherObject.collisionType == kCollisionTypeCircle) {
      float dist = sqrt(pow(self.position.x - otherObject.position.x, 2) +
                        pow(self.position.y - otherObject.position.y, 2));
      if (dist <= self.collisionSize / 2 + otherObject.collisionSize / 2) {
        return YES;
      }
    }
  }
  return NO;
}

- (void)updateStateWithDeltaTime:(ccTime)deltaTime
            andListOfGameObjects:(CCArray *)listOfGameObjects {
  if (self.state == kObjectStateExit) {
    return;
  } else if (self.state == kObjectStateMoving) {
    for (GameObject *go in listOfGameObjects) {
      if (go.tag == kDragonHeadTag) {
        if (sqrtf(powf(go.position.x - self.position.x, 2) +
                  powf(go.position.y - self.position.y, 2)) > 1000) {
          [self changeState:kObjectStateExit];
          return;
        }
      }
      if (go.tag == kEggShellTag && go.state == kObjectStateIdle) {
        if ([self collidesWith:go]) {
          CCScene *runningScene = [CCDirector sharedDirector].runningScene;
          GameLayer *gameLayer = (GameLayer *)[runningScene getChildByTag:kGameLayerTag];
          if (gameLayer && ((EggShell *)go).eggType == kEggTypeGood) {
            [gameLayer createObjectOfType:kObjectTypeFriedEgg
                               atLocation:go.position
                             withVelocity:CGPointZero
                               withZValue:kFriedEggZValue];
          } else {
            CCLOG(@"Error retrieving game layer while updating Fireball.");
          }
          [self changeState:kObjectStateExit];
          [go changeState:kObjectStateBreaking];
        }
      }
    }
    
    [self moveWithDelta:deltaTime];
  }
}

- (void)startAnimations {
  [_movingAnim autorelease];
  _movingAnim = [[self loadPlistForAnimationWithName:@"movingAnim"
                                       andEntityName:@"fireball"] retain];
}

@end
