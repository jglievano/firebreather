//
//  DragonTail.h
//  firebreather
//
//  Created by Gabriel Lievano on 6/18/12.
//  Copyright 2012 Eosch. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameObject.h"

@interface DragonTail : GameObject

@property (nonatomic, retain) GameObject *target;
@property (nonatomic, assign) BOOL isGrown;

@end
