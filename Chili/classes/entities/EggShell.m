//
//  EggShell.m
//  firebreather
//
//  Created by Gabriel Lievano on 6/16/12.
//  Copyright 2012 Eosch. All rights reserved.
//

#import "EggShell.h"

@implementation EggShell {
  CCAnimation *_idleAnim;
  CCAnimation *_breakingAnim;
}

- (id)init {
  self = [super initWithSpriteFrameName:@"breaking_egg_1.png"];
  if (self) {
    [self startAnimations];
    self.isActive = NO;
    self.eggType = kEggTypeGood;
    self.gameObjectType = kObjectTypeEggShell;
    self.speed = 0.0;
    self.collisionType = kCollisionTypeCircle;
    self.collisionSize = 30;
    self.opacity = 0;
    [self changeState:kObjectStateIdle];
    CCCallBlock *completionBlock = [CCCallBlock actionWithBlock:^{
      self.isActive = YES;
      self.state = kObjectStateIdle;
    }];
    CCFadeIn *fadeIn = [CCFadeIn actionWithDuration:2.0];
    CCSequence *seq = [CCSequence actions:fadeIn, completionBlock, nil];
    [self runAction:seq];
  }
  return self;
}

- (void)setEggType:(EggType)eggType {
  _eggType = eggType;
  if (_eggType == kEggTypeRock) {
    CCAnimation *rockAnim = [self loadPlistForAnimationWithName:@"rockAnim"
                                                  andEntityName:@"egg_shell"];
    [self runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:rockAnim]]];
    [self runAction:[CCRepeatForever actionWithAction:
        [CCRotateBy actionWithDuration:0.5 angle:360]]];
  } else if (_eggType == kEggTypeRotten) {
    self.color = ccc3(86, 129, 35);
  }
}

- (void)dealloc {
  [_idleAnim release];
  [_breakingAnim release];
  [super dealloc];
}

- (void)changeState:(ObjectState)newState {
  if (!self.isActive) {
    return;
  }
  // If it is a rock egg, then it should never change state.
  if (_eggType == kEggTypeRock) {
    return;
  }
  [self stopAllActions];
  id action = nil;
  self.state = newState;
  
  if (newState == kObjectStateIdle) {
    action = [self sequenceWithAnimation:_idleAnim andCompletionBlock:^{
      [self changeState:kObjectStateIdle];
    }];
  } else if (newState == kObjectStateBreaking) {
    action = [self sequenceWithAnimation:_breakingAnim andCompletionBlock:^{
      [self changeState:kObjectStateExit];
    }];
  } else {
    CCLOG(@"Unhandled state in EggShell");
  }
  
  if (action) {
    [self runAction:action];
  }
}

- (void)updateStateWithDeltaTime:(ccTime)deltaTime
            andListOfGameObjects:(CCArray *)listOfGameObjects {
  if (self.state == kObjectStateDead) {
    return;
  } else if (self.state == kObjectStateIdle) {

  }
}

- (void)startAnimations {
  [_idleAnim autorelease];
  _idleAnim = [[self loadPlistForAnimationWithName:@"idleAnim"
                                     andEntityName:@"egg_shell"] retain];
  [_breakingAnim autorelease];
  _breakingAnim = [[self loadPlistForAnimationWithName:@"breakingAnim"
                                         andEntityName:@"egg_shell"] retain];
}

#pragma - Public methods.

- (BOOL)isEdible {
  return self.state != kObjectStateIdle;
}

@end
