//
//  GameObject.m
//  firebreather
//
//  Created by Gabriel Lievano on 6/18/12.
//  Copyright (c) 2012 Eosch. All rights reserved.
//

#import "GameObject.h"

@implementation GameObject

@synthesize reactsToScreenBoundaries = _reactsToScreenBoundaries;
@synthesize screenSize = _screenSize;
@synthesize isActive = _isActive;
@synthesize state = _state;
@synthesize velocity = _velocity;
@synthesize speed = _speed;
@synthesize gameObjectType = _gameObjectType;
@synthesize collisionType = _collisionType;
@synthesize collisionSize = _collisionSize;

- (id)init {
  self = [super init];
  if (self) {
    CCLOG(@"GameObject init");
    _screenSize = [CCDirector sharedDirector].winSize;
    _isActive = YES;
    _velocity = CGPointZero;
    _gameObjectType = kObjectTypeNone;
    _collisionType = kCollisionTypeNone;
  }
  return self;
}

- (void)changeState:(ObjectState)newState {
  CCLOG(@"GameObject::changeState:(ObjectState) method should be overriden");
}

- (void)updateStateWithDeltaTime:(ccTime)deltaTime andListOfGameObjects:(CCArray *)listOfGameObjects {
  CCLOG(@"GameObject::updateStateWithDeltaTime:(ccTime) andListOfGameObjects:(CCArray*) should be overriden");
}

- (CCSequence *)sequenceWithAnimation:(CCAnimation *)anim
                   andCompletionBlock:(AnimCompletionBlock)block {
  CCCallBlock *completionBlock = [CCCallBlock actionWithBlock:block];
  CCAnimate *animation = [CCAnimate actionWithAnimation:anim];
  CCSequence *seq = [CCSequence actions:animation, completionBlock, nil];
  return seq;
}

- (CGRect)adjustBoundingBox {
  CCLOG(@"GameObject::adjustBoundingBox should be overriden");
  return self.boundingBox;
}

- (BOOL)collidesWith:(GameObject *)otherObject {
  [self doesNotRecognizeSelector:_cmd];
  return NO;
}

- (CCAnimation *)loadPlistForAnimationWithName:(NSString *)animationName
                                 andEntityName:(NSString *)entityName {
  CCAnimation *animationToReturn = nil;
  NSString *fullFileName = [NSString stringWithFormat:@"%@.plist", entityName];
  NSString *plistPath;
  
  // Get the path to the plist file
  NSString *rootPath =
      [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)
          objectAtIndex:0];
  plistPath = [rootPath stringByAppendingPathComponent:fullFileName];
  if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
    plistPath = [[NSBundle mainBundle] pathForResource:entityName ofType:@"plist"];
  }
  
  // Read in the plist file
  NSDictionary *plistDictionary = [NSDictionary dictionaryWithContentsOfFile:plistPath];
  
  // If the plistDictionary is null, the file was not found
  if (!plistDictionary) {
    CCLOG(@"Error reading plist: %@.plist", entityName);
    return nil;
  }
  
  // Get just the mini-dictionary for this animation
  NSDictionary *animationSettings = [plistDictionary objectForKey:animationName];
  if (!animationSettings) {
    CCLOG(@"Could not locate animation with name: %@", animationName);
    return nil;
  }
  
  // Get the delay value for the animation
  float animationDelay = [[animationSettings objectForKey:@"delay"] floatValue];
  animationToReturn = [CCAnimation animation];
  [animationToReturn setDelayPerUnit:animationDelay];
  
  // Add the frames to the animation
  NSString *animationFramePrefix = [animationSettings objectForKey:@"filenamePrefix"];
  NSString *animationFrames = [animationSettings objectForKey:@"animationFrames"];
  NSArray *animationFrameNumbers = [animationFrames componentsSeparatedByString:@","];
  
  for (NSString *frameNumber in animationFrameNumbers) {
    NSString *frameName = [NSString stringWithFormat:@"%@%@.png", animationFramePrefix, frameNumber];
    [animationToReturn addSpriteFrame:[[CCSpriteFrameCache sharedSpriteFrameCache]
        spriteFrameByName:frameName]];
  }
  
  return animationToReturn;
}

#pragma mark -
#pragma mark Motion Methods

- (void)lookAt:(CGPoint)target {
  CGPoint eye = [self position];
  CGPoint current = CGPointMake(eye.x, eye.y + 1);
  
  CGPoint nTarget = CGPointMake(target.x - eye.x, target.y - eye.y);
  CGPoint nCurrent = CGPointMake(current.x - eye.x, current.y - eye.y);
  
  float nTargetM = sqrt((nTarget.x * nTarget.x) + (nTarget.y * nTarget.y));
  float nCurrentM = sqrt((nCurrent.x * nCurrent.x) + (nCurrent.y * nCurrent.y));
  
  nTarget = CGPointMake(nTarget.x / nTargetM, nTarget.y / nTargetM);
  nCurrent = CGPointMake(nCurrent.x / nCurrentM, nCurrent.y / nCurrentM);
  
  float dotp = (nCurrent.x * nTarget.x) + (nCurrent.y * nTarget.y);
  float r = acos(dotp);
  if (nTarget.x < 0) r = (M_PI * 2) - r;
  [self rotate:r];
}

- (void)rotate:(float)r {
  [self setRotation:CC_RADIANS_TO_DEGREES(r)];
}

- (void)velocityTowards:(CGPoint)target {
  CGPoint diff = CGPointMake(target.x - [self position].x, target.y - [self position].y);
  [self setVelocity:diff];
}

- (void)moveWithDelta:(ccTime)deltaTime {
    // Move object (velocity will be treated only as direction)
  if (!CGPointEqualToPoint([self velocity], CGPointZero)) {
    float mA = sqrt(([self velocity].x * [self velocity].x) +
                    ([self velocity].y * [self velocity].y));
    CGPoint nA = CGPointMake([self velocity].x / mA, [self velocity].y / mA);
    CGPoint currP = [self position];
    float nextX = currP.x + (nA.x * [self speed] * deltaTime);
    float nextY = currP.y + (nA.y * [self speed] * deltaTime);
    
    [self lookAt:CGPointMake(nextX + (nA.x * 100), nextY + (nA.y * 100))];
    
    nextX = currP.x + (sin(CC_DEGREES_TO_RADIANS([self rotation])) * [self speed] * deltaTime);
    nextY = currP.y + (cos(CC_DEGREES_TO_RADIANS([self rotation])) * [self speed] * deltaTime);
    
    self.position = CGPointMake(nextX, nextY);
  }
}

@end
