//
//  Crash.m
//  Chili
//
//  Created by Gabriel Liévano on 2/22/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import "Crash.h"

@implementation Crash

@synthesize reason;

- (id)init {
  self = [super initWithSpriteFrameName:@"crash_1.png"];
  if (self) {
    self.gameObjectType = kObjectTypeNone;
    self.speed = 0.0;
    self.scale = 1.0;
    [self changeState:kObjectStateIdle];
  }
  return self;
}

- (void)changeState:(ObjectState)newState {
  [self stopAllActions];
  self.state = newState;

  if (newState == kObjectStateIdle) {
    CCScaleTo *scaleUp = [CCScaleTo actionWithDuration:0.1 scale:2.0];
    CCScaleTo *scaleDown = [CCScaleTo actionWithDuration:0.3 scale:0.1];
    CCCallBlock *block = [CCCallBlock actionWithBlock:^{
      [self changeState:kObjectStateExit];
    }];
    CCSequence *seq = [CCSequence actionWithArray:@[scaleUp, scaleDown, block]];
    [self runAction:seq];
  }
}

- (void)updateStateWithDeltaTime:(ccTime)deltaTime
            andListOfGameObjects:(CCArray *)listOfGameObjects {
  if (self.state == kObjectStateDead) {
    return;
  } else if (self.state == kObjectStateIdle) {

  }
}

@end
