//
//  EggShell.h
//  firebreather
//
//  Created by Gabriel Lievano on 6/16/12.
//  Copyright 2012 Eosch. All rights reserved.
//

#import "GameObject.h"

typedef NS_ENUM(NSInteger, EggType) {
  kEggTypeGood,
  kEggTypeRock,
  kEggTypeRotten
};

@interface EggShell : GameObject

@property (nonatomic, assign) EggType eggType;
- (BOOL)isEdible;

@end
