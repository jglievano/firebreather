//
//  DragonHead.m
//  firebreather
//
//  Created by Gabriel Lievano on 6/16/12.
//  Copyright 2012 Eosch. All rights reserved.
//

#import "DragonHead.h"

#import "Constants.h"
#import "DragonTail.h"
#import "EggShell.h"

@implementation DragonHead {
  CCAnimation *_movingAnim;
}

@synthesize fedFood = _fedFood;

- (id)init {
  self = [super initWithSpriteFrameName:@"dragonhead_1.png"];
  if (self) {
    [self startAnimations];
    self.gameObjectType = kObjectTypeDragonHead;
    self.speed = 120.0;
    self.collisionSize = 40.0f;
    self.collisionType = kCollisionTypeCircle;
    _fedFood = 0;
    [self changeState:kObjectStateMoving];
  }
  return self;
}

- (void)dealloc {
  [_movingAnim release];
  [super dealloc];
}

- (void)changeState:(ObjectState)newState {
  [self stopAllActions];
  id action = nil;
  self.state = newState;
  
  if (newState == kObjectStateMoving) {
    action = [CCAnimate actionWithAnimation:_movingAnim];
  } else if (newState == kObjectStateDead) {
    CCLOG(@"Dragon is dead, next will be game over.");
  } else {
    CCLOG(@"Unhandled state in DragonHead");
  }
  
  if (action) {
    [self runAction:action];
  }
}

- (BOOL)collidesWith:(GameObject *)otherObject {
    // this is type circle
  if (CGRectIntersectsRect(self.boundingBox, otherObject.boundingBox)) {
    if (otherObject.collisionType == kCollisionTypeCircle) {
      float dist = sqrt(pow(self.position.x - otherObject.position.x, 2) +
                        pow(self.position.y - otherObject.position.y, 2));
      if (dist <= self.collisionSize / 2 + otherObject.collisionSize / 2) {
        return YES;
      }
    }
  }
  return NO;
}

- (void)updateStateWithDeltaTime:(ccTime)deltaTime
            andListOfGameObjects:(CCArray *)listOfGameObjects {
  if (self.state == kObjectStateDead) {
    return;
  }
  else if (self.state == kObjectStateMoving) {
    [self moveWithDelta:deltaTime];
    for (GameObject *go in listOfGameObjects) {
      if (go.tag == kFriedEggTag && go.state == kObjectStateIdle) {
        if ([self collidesWith:go]) {
          _fedFood++;
          CCLOG(@"Fed food: %d", _fedFood);
          [[NSNotificationCenter defaultCenter] postNotificationName:kStrDragonGrow object:nil];
          [go changeState:kObjectStateExit];
        }
      } else if (go.tag == kDragonTailTag) {
        if (((DragonTail *)go).isGrown) {
          if ([self collidesWith:go]) {
            [self changeState:kObjectStateDead];
          }
        }
      } else if (go.tag == kEggShellTag) {
        if ([self collidesWith:go]) {
          if (!((EggShell *)go).isEdible && go.isActive) {
            [self changeState:kObjectStateDead];
          }
        }
      }
    }
  }
}

- (void)startAnimations {
  [_movingAnim autorelease];
  _movingAnim = [[self loadPlistForAnimationWithName:@"movingAnim"
                                       andEntityName:@"dragon_head"] retain];
}

@end
