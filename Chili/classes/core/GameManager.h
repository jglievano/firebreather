//
//  GameManager.h
//  firebreather
//
//  Created by Gabriel Lievano on 11/3/12.
//
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface GameManager : NSObject {
  SceneType currentScene;
}

@property (readwrite) BOOL isMusicOn;
@property (readwrite) BOOL isSoundFXOn;
@property (readwrite) BOOL hasPlayerDied;
@property (nonatomic, readonly) NSString *currentLevel;

+ (GameManager *)sharedManager;
- (void)runSceneWithID:(SceneType)sceneID;

- (void)pauseGame;
- (void)resumeGame;
- (void)restartGame;

- (void)reportScore:(int)score toLeaderboard:(NSString *)lid;
- (void)showLeaderboard;

- (BOOL)shouldShowTutorialStep:(int)step;
- (void)registerTutorialStepHasBeenShown:(int)step;

@end
