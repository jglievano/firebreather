//
//  ResourceManager.m
//  Chili
//
//  Created by Gabriel Liévano on 8/3/13.
//  Copyright (c) 2013 Eosch. All rights reserved.
//

#import "ResourceManager.h"

@implementation ResourceManager

+ (ResourceManager *)sharedManager {
  static ResourceManager *_sharedManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _sharedManager = [[self alloc] init];
  });
  return _sharedManager;
}

@end
