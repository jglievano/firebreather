//
//  GameManager.m
//  firebreather
//
//  Created by Gabriel Lievano on 11/3/12.
//
//

#import "GameManager.h"

#import "AppDelegate.h"
#import "GameLayer.h"
#import "MainMenuLayer.h"
#import "StringConstants.h"

#import "LocalyticsSession.h"

@implementation GameManager

static NSString * const kStrLGamePaused = @"Game Paused";
static NSString * const kStrLGameResumed = @"Game Resume";
static NSString * const kStrLGameRestarted = @"Game Restart";
static NSString * const kStrLMainMenu = @"Opening Main Menu";

@synthesize isMusicOn;
@synthesize isSoundFXOn;
@synthesize hasPlayerDied;

+ (GameManager *)sharedManager {
  static GameManager *_sharedManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _sharedManager = [[self alloc] init];
  });
  return _sharedManager;
}

- (id)init {
  self = [super init];
  if (self) {
    CCLOG(@"<GameManager> init");
    isMusicOn = YES;
    isSoundFXOn = YES;
    hasPlayerDied = NO;
    currentScene = kNoSceneUnininitialized;
  }
  return self;
}

- (void)runSceneWithID:(SceneType)sceneID {
  SceneType oldScene = currentScene;
  currentScene = sceneID;
  id sceneToRun = nil;
  switch (sceneID) {
    case kMainMenuScene:
      [[LocalyticsSession shared] tagEvent:kStrLMainMenu];
      sceneToRun = [MainMenuLayer scene];
      break;
    case kOptionsScene:
      break;
    case kIntroScene:
      break;
    case kCreditsScene:
      break;
    case kGameLevelDojo:
      sceneToRun = [GameLayer sceneWithLevel:kStrDojo];
      break;
    case kGameLevelImperialGardens:
      sceneToRun = [GameLayer sceneWithLevel:kStrImperialGardens];
      break;
    case kGameLevelCourtyard:
      sceneToRun = [GameLayer sceneWithLevel:kStrCourtyard];
      break;
    default:
      CCLOG(@"<GameManager> runSceneWithID:SceneType unknown ID, cannot switch scenes");
      return;
      break;
  }
  
  if (sceneToRun == nil) {
    // Nothing has changed, revert
    currentScene = oldScene;
    return;
  }
  
  // TODO: we might need some kind of scaling to manage support devices here.
  // Howto? [sceneToRun setScaleX:scaleX]; [sceneToRun setScaleY:scaleY];
  
  if ([CCDirector sharedDirector].runningScene == nil) {
    [[CCDirector sharedDirector] runWithScene:sceneToRun];
  } else {
    [[CCDirector sharedDirector] replaceScene:sceneToRun];
  }
}

- (void)pauseGame {
  if (currentScene >= kGameLevelDojo && currentScene <= kGameLevelCourtyard) {
    [[LocalyticsSession shared] tagEvent:kStrLGamePaused];
    GameLayer *layer = (GameLayer *)[[CCDirector sharedDirector].runningScene
        getChildByTag:kGameLayerTag];
    [layer pause];
  }
}

- (void)resumeGame {
  if (currentScene >= kGameLevelDojo && currentScene <= kGameLevelCourtyard) {
    [[LocalyticsSession shared] tagEvent:kStrLGameResumed];
    GameLayer *layer = (GameLayer *)[[CCDirector sharedDirector].runningScene
                                     getChildByTag:kGameLayerTag];
    [layer resume];
  }
}

- (void)restartGame {
  [[LocalyticsSession shared] tagEvent:kStrLGameRestarted];
  [self runSceneWithID:currentScene];
}

- (BOOL)shouldShowTutorialStep:(int)step {
  NSString *tutorialStepKey = [NSString stringWithFormat:kStrTutorialStep, step];
  BOOL showTutorial =
      [[[NSUserDefaults standardUserDefaults] objectForKey:tutorialStepKey] boolValue];
  return showTutorial;
}

- (void)registerTutorialStepHasBeenShown:(int)step {
  if (step > 0 && step < 4) {
    NSString *tutorialStepKey = [NSString stringWithFormat:kStrTutorialStep, step];
    [[NSUserDefaults standardUserDefaults] setObject:@(YES)
                                              forKey:tutorialStepKey];
  }
}

- (NSString *)currentLevel {
  switch (currentScene) {
    case kGameLevelDojo:
      return @"dojo";
      break;
    case kGameLevelCourtyard:
      return @"courtyard";
      break;
    case kGameLevelImperialGardens:
      return @"imperialgardens";
    default:
      break;
  }
  return @"unknown";
}

- (void)reportScore:(int)score toLeaderboard:(NSString *)lid {
  CCLOG(@"Uploading to %@", lid);
  [[GameCenterManager sharedManager] saveAndReportScore:score
                                            leaderboard:lid
                                              sortOrder:GameCenterSortOrderHighToLow];
}

- (void)showLeaderboard {
  AppController *app = (AppController *)[UIApplication sharedApplication].delegate;
  [app showLeaderboard];
}

@end
