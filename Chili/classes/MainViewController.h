//
//  MainViewController.h
//  Chili
//
//  Created by Gabriel Liévano on 2/22/14.
//  Copyright (c) 2014 Eosch. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GameCenterManager.h"

@interface MainViewController : UINavigationController
<GameCenterManagerDelegate>

- (void)showLeaderboard;

@end
