#!/usr/bin/python

#  generate_strings.py
#  Chili
#
#  Created by Gabriel Lievano on 8/3/13.
#  Copyright (c) 2013 Eosch. All rights reserved.

"""generate-strings.py: Takes Localizable.strings file and generates a file with all
  strings identified by a string called StringConstants.h"""

__author__ = "Gabriel Lievano"
__copyright__ = "Copyright (c) 2013 Eosch. All rights reserved."

import re

# StringConstants is based on English language.
infile = open('Chili/Resources/data/en.lproj/Localizable.strings', 'r')
outfile = open('Chili/classes/common/StringConstants.h', 'w')

outfile.write('//\n//  StringConstants.h\n//  Chili\n//\n')
outfile.write('//  Auto-generated file using generate-strings.py\n//\n')
outfile.write('//  Copyright (c) 2013 Eosch. All rights reserved.\n//\n\n')

regex = r'^[A-Za-z0-9-_"]* = [A-Za-z0-9-_"]*'
line_pattern = re.compile(regex)
for line in infile:
  if re.match(line_pattern, line):
    outfile.write('static NSString * const ')
    key = ''
    word = ''
    quote_count = 0
    to_upper = True
    for c in line:
      if c == '"':
        quote_count += 1
        if quote_count >= 2:
          break
      elif c == '_':
        key += c
        to_upper = True
        continue
      else:
        key += c
        if to_upper:
          word += c.upper()
          to_upper = False
        else:
          word += c.lower()
    outfile.write('kStr' + word + ' = @\"' + key + '\";')
    outfile.write('\n')

infile.close()

