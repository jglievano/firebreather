//
//  AppDelegate.m
//  Chili
//
//  Created by Gabriel Liévano on 4/21/13.
//  Copyright Eosch 2013. All rights reserved.
//

#import "cocos2d.h"

#import "AppDelegate.h"
#import "GameManager.h"
#import "IntroLayer.h"

#import "GameCenterManager.h"
#import "LocalyticsSession.h"

#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)
#define LOCALYTICS_KEY @ STRINGIZE2(LOCALYTICS)

@implementation AppController

@synthesize window=_window, navController=_navController, director=_director;

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  [[GameCenterManager sharedManager] setupManager];

	// Create the main window.
	_window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

	// Create an CCGLView with a RGB565 color buffer, and a depth buffer of 0-bits.
	CCGLView *glView = [CCGLView viewWithFrame:[_window bounds]
             pixelFormat:kEAGLColorFormatRGB565	//kEAGLColorFormatRGBA8
             depthFormat:0	//GL_DEPTH_COMPONENT24_OES
      preserveBackbuffer:NO
              sharegroup:nil
           multiSampling:NO
         numberOfSamples:0];

	_director = (CCDirectorIOS *)[CCDirector sharedDirector];
	_director.wantsFullScreenLayout = YES;

  _director.displayStats = NO;
  _director.animationInterval = 1.0 / 60;
  _director.view = glView;
  _director.delegate = self;
  _director.projection = kCCDirectorProjection2D;
  
	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices.
	if (![_director enableRetinaDisplay:YES]) {
		CCLOG(@"Retina Display Not supported");
  }

	// Default texture format for PNG/BMP/TIFF/JPEG/GIF images.
	// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
	// You can change anytime.
	[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];

	// If the 1st suffix is not found and if fallback is enabled then fallback suffixes are going to
  // searched. If none is found, it will try with the name without suffix.
	// On iPad HD  : "-ipadhd", "-ipad",  "-hd"
	// On iPad     : "-ipad", "-hd"
	// On iPhone HD: "-hd"
	CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
  sharedFileUtils.enableFallbackSuffixes = NO;          // Default: NO.
  sharedFileUtils.iPhoneRetinaDisplaySuffix = @"-hd";   // Default: "-hd"
  sharedFileUtils.iPadSuffix = @"-ipad";                // Default: "-ipad"
  sharedFileUtils.iPadRetinaDisplaySuffix = @"-ipadhd"; // Default: "-ipadhd"

	// Assume that PVR images have premultiplied alpha.
	[CCTexture2D PVRImagesHavePremultipliedAlpha:YES];

	// and add the scene to the stack. The director will run it when it automatically when the view is
  // displayed.
	[_director pushScene:[IntroLayer scene]];
	
	// Create a Navigation Controller with the Director.
	_navController = [[MainViewController alloc] initWithRootViewController:_director];
	_navController.navigationBarHidden = YES;
	
	// set the Navigation Controller as the root view controller.
	[_window setRootViewController:_navController];
	
	// make main window visible.
	[_window makeKeyAndVisible];
	
	return YES;
}

// Supported orientations: Landscape. Customize it for your own needs.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

// Getting a call, pause the game.
- (void)applicationWillResignActive:(UIApplication *)application {
  [[LocalyticsSession shared] close];
  [[LocalyticsSession shared] upload];
	if ([_navController visibleViewController] == _director) {
		[_director pause];
  }
}

// Call got rejected
- (void)applicationDidBecomeActive:(UIApplication *)application {
  [[LocalyticsSession shared] LocalyticsSession:LOCALYTICS_KEY];
  [[LocalyticsSession shared] resume];
  [[LocalyticsSession shared] upload];
	if ([_navController visibleViewController] == _director) {
		[_director resume];
  }
}

- (void)applicationDidEnterBackground:(UIApplication*)application {
  [[LocalyticsSession shared] close];
  [[LocalyticsSession shared] upload];
	if ([_navController visibleViewController] == _director) {
		[_director stopAnimation];
  }
}

- (void)applicationWillEnterForeground:(UIApplication*)application {
  [[LocalyticsSession shared] resume];
  [[LocalyticsSession shared] upload];
	if ([_navController visibleViewController] == _director) {
		[_director startAnimation];
  }
}

// Application will be killed.
- (void)applicationWillTerminate:(UIApplication *)application {
  [[LocalyticsSession shared] close];
  [[LocalyticsSession shared] upload];
	CC_DIRECTOR_END();
}

// Purge memory.
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[[CCDirector sharedDirector] purgeCachedData];
}

// Next delta time will be zero.
- (void)applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void)dealloc {
	[_window release];
	[_navController release];

	[super dealloc];
}

- (void)showLeaderboard {
  [_navController showLeaderboard];
}

@end
