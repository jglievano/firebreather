//
//  AppDelegate.h
//  Chili
//
//  Created by Gabriel Liévano on 4/21/13.
//  Copyright Eosch 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MainViewController.h"
#import "cocos2d.h"

@interface AppController : NSObject<UIApplicationDelegate, CCDirectorDelegate> {
	UIWindow *_window;
	MainViewController *_navController;

	CCDirectorIOS	*_director;
}

@property (nonatomic, retain) UIWindow *window;
@property (readonly) MainViewController *navController;
@property (readonly) CCDirectorIOS *director;

- (void)showLeaderboard;

@end
